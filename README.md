# GenstagePubsub

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `genstage_pubsub` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:genstage_pubsub, "~> 0.1.0"}]
    end
    ```

  2. Ensure `genstage_pubsub` is started before your application:

    ```elixir
    def application do
      [applications: [:genstage_pubsub]]
    end
    ```

