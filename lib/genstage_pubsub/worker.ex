defmodule GenstagePubsub.Worker do
  use GenServer

  @events [
    %{type: "new_entry", data: %{id: 1, name: "foo"}},
    %{type: "new_entry", data: %{id: 2, name: "bar"}},
    %{type: "new_entry", data: %{id: 3, name: "baz"}},
    %{type: "new_entry", data: %{id: 4, name: "gamma"}},
    %{type: "new_entry", data: %{id: 5, name: "beta"}},
    %{type: "new_entry", data: %{id: 6, name: "delta"}}
  ]


  def start_link() do
    GenServer.start_link(__MODULE__, [])
  end

  def init(_) do
    send(self, :generate)
    {:ok, []}
  end

  def handle_info(:generate, state) do
    @events
    |> Enum.random
    |> GenstagePubsub.Publisher.publish

    Process.send_after(self, :generate, Enum.random(1..100))
    {:noreply, state}
  end
end