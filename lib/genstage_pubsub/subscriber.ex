defmodule GenstagePubsub.Subscriber do
  use GenStage
  @derive [Poison.Encoder]

  def start_link(name, publisher) do
    GenStage.start_link(__MODULE__, [name, publisher])
  end

  def init([name, publisher]) do
    # Starts a permanent subscription to the broadcaster
    # which will automatically start requesting items.
    {:consumer, name, subscribe_to: [publisher]}
  end

  def handle_events(events, _from, name) do
    Enum.each(events, fn event ->
      IO.inspect "Subscriber #{name} received: #{Poison.encode! event}"
    end)
    {:noreply, [], name}
  end
end