defmodule GenstagePubsub.Supervisor do
  use Supervisor

  @collector_name GenstagePubsub.Collector
  @publisher_name GenstagePubsub.Publisher

  def start_link do
    Supervisor.start_link(__MODULE__, [])
  end

  def init([]) do
    children = [
      worker(GenstagePubsub.Publisher, []),
      worker(GenstagePubsub.Subscriber, ["A", @publisher_name], id: "1"),
      worker(GenstagePubsub.Subscriber, ["B", @publisher_name], id: "2"),
      worker(GenstagePubsub.Subscriber, ["C", @publisher_name], id: "3"),
      worker(GenstagePubsub.Worker, [])
    ]

    supervise(children, strategy: :one_for_one)
  end
end